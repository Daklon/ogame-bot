argh==0.26.2
configparser==3.5.0
pathtools==0.1.2
PyYAML==3.12
selenium==3.14.0
urllib3==1.23
watchdog==0.8.3
