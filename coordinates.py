class Coordinates():
    #class constructor, it sets the default value to an imposiible default value
    def __init__(self,coords='99:999:9'):
        self.coords = []
        coords = coords.replace('[','')
        coords = coords.replace(']','')
        for coord_scale in coords.split(':'):
            self.coords.append(int(coord_scale))

    #returns the nearest planet to self, given a list of possible candidates
    #if there is no candidate, returns itself
    def get_nearest(self,toCoords = []):
        nearest = Coordinates()
        for coord in toCoords:
            print('for, self, coord, nearest:' + str(self)+ ' ' +str(coord) + ' ' + str(nearest))
            if ((self - coord < self - nearest) and (self != coord)):
                print("enterif")
                nearest = coord
        if nearest == '99:999:9':
            return self
        else:
            return nearest
    
    #comparision coordinates method, if one is less than other
    def __lt__(self, other):
        if self[0] < other[0]:
            return True
        elif self[0] == other[0]:
            if self[1] < other[1]:
                return True
            elif self[1] == other[1]:
                if self[2] < other[2]:
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False

    #comparision coordinates method, if equal
    def __eq__(self, other):
        if (self[0] == other[0] and
                self[1] == other[1] and
                self[2] == other[2]):
            return True
        else:
            return False

    #allows to acces to the galaxy,system or planet using coord[]
    def __getitem__(self,key):
        return self.coords[key]
    #allows to set the galaxy,system or planet using coord[]
    def __setitem__(self,key,value):
        self.coords[key] = value

    #substraction method, its able to substract coordinates and return the diference
    def __sub__(self, other):
        result = Coordinates()
        result[0] = abs(self[0] - other[0])
        result[1] = abs(self[1] - other[1])
        result[2] = abs(self[2] - other[2])
        return result

    #returns the coord item in string form
    def __str__(self):
        return (str(self[0]) + ':' + str(self[1]) + ':' + str(self[2]))
